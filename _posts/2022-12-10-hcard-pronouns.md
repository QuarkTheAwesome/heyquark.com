---
title: h-card really needs to fix their whole gender thing
tags: programming gender vent
comments: true
---

I have a h-card on my [aboutme](/aboutme) page, since it's a nice way to mark up the info I have anyway, without having to duplicate it all in the way some other structured data formats want. Also, if it can be slightly less friendly to Google, that's a plus in my book. I s'pose the idea is that someone's feedreader can have a little card with info about me next to my posts, which is cute, but I'm not actually aware of anything that does this. In any case, I was making some little tweaks to it and was tagging up my pronouns and gender, which, wow, what an experience!

First off, there is no standard for tagging your pronouns, which is a little weird. One of the best things about HTML and formats like it that adopt the same philosophy, though, is that you can just invent tags and UAs will be cool about it until the standards bodies catch up. So, `u-pronoun` is a thing that exists, and works okay. I used to have a pronoun.is link there, but I think that got caught up in the Great Heroku Paywalling of 2022.

- [The Pronouns in my h-card ¶ Zegnåt](https://wiki.zegnat.net/microformats/pronoun)
- [Heroku's Next Chapter](https://blog.heroku.com/next-chapter#focus-on-mission-critical) - yes this is really the best annoucement of this I can find, yes they're really doing this so they can work on *mTLS*. mTLS!!

The only pain point here is that there isn't a good answer for certain multi-pronoun drifters like myself. I use she/her and they/them (at time of writing {{ "wink" | m_emoji }}) and prefer to write that as "she/they". So:
```html
<abbr class="u-pronoun" title="she/her or they/them">she/they</abbr>
```
Works okay, but that " or " isn't very parser-friendly.
```html
<abbr title="she/her or they/them">
    <span class="u-pronoun" title="she/her">she</span>
    /
    <span class="u-pronoun" title="they/them">they</span>
</abbr>
```
Better for machine parsing, though I have no idea how browsers will take nested titles like that. Since it's more important that Humans know what's up, and I just don't like the markup, I have option #1 for now, but it's a bit meh.

At least my hypothetical RSS reader user knows how to address me in their comment, though. You know what's sitting there entirely untagged?
```html
<abbr title="non-binary">enby</abbr>
```
Should be an easy fix, right? The spec, which is a wiki page, has two gender-related tags to use:
- `p-gender-identity` - gender identity, new in vCard4 (RFC 6350)
- `p-sex` - biological sex, new in- huh-wait, what? Ow.

So, like, this obviously sucks. I think the intent is for me to put the `p-gender-identity` class on my example tag, but I am straght-up not going to do that since it draws attention to the fact I *didn't* tag `p-sex`. "Biological sex" is a fake thing used to find out what trans people "really" are, and I'm sorry to tell you that I am *really* non-binary. I'm not even sure how I *would* tag such a thing - I don't know my chromosomal situation (along with the vast majority of people) nor have I undergone hormonal testing. If what you *actually* want to know about is my genitals or childbearing ability, then, wow, at least buy me dinner or flirt a little first?? I'm not just publishing that on my main website for you to gawk at. Literally none of this impacts anyone outside my long-term partners and doctor anyway, so the Microformats people might as well standardise tags for blood type and kidney functionality while they're at it.

- [A thread about "biological sex" from biology professor Rebecca Helm](https://farside.link/nitter/RebeccaRHelm/status/1207834357639139328)
- [h-card - Microformats wiki](https://microformats.org/wiki/index.php?title=h-card&oldid=70590) - permalink in case they change this, which they should

So, y'know, both of these tags should be dropped - I have a gender, and that's just how it is. Nice and simple! `p-gender`. Or is it `u-gender`?

Stay fresh.
