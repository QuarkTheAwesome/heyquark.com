---
title: Introductions
category: me
tags: sensitive vent
comments: true
---

(So, this post is a little different. It's highly personal and a bit of a vent; about part of my experience while doing something as "simple" as introductions. It's a stream of conciousness and thus could be triggering. So, big **content warning** for transgender anxieties. If you're trans, you'll probably relate to this and it could bring up some upsetting memories. If you're looking for a page that introduces myself, that's [About Me](/aboutme).)

## Conversation #1

"Hi, what was your name?"

Okay. I haven't met this person before, so that's cool. My name's Ash, so I should respond with that, but I'm here with my family and I'm not out to them, so if I use Ash with this person there's a chance that could spread back to my parents who will confront me about it later. I reaaaallly don't want to deal with *that* whole mess, though I guess since I'm starting to become financially independent I could soon.. but not now, not here, god no. Wait, how long have I been thinking for? Are they going to think I'm making up a fake name? I guess that *is* what I'm doing. I don't think it's long enough to be awkward, at least...

"Hi, I'm [deadname]. Nice to meet you."

That felt pretty terrible. Been a little while since I've had to do that.

## Conversation #2

"I swear I know this guy from somewhere..."

Um, you do? You're *vaguely* familiar at best. Where did we meet? *What name did I use?*

"Oh, right! I interviewed you when you applied to a job at..."

Shit. Shit shit shit. All my job applications during uni were hours of travel away from the parents, thus I didn't see a need to use my deadname - they almost certainly know me as Ash. I have just finished a conversation where I introduced myself as not that, so if this other person comes back, or the two of them talk later, or they both talk to the same third party, this could be a worst-case scenario; they're gonna try and figure out if they're thinking of the same person, which is gonna involve lots of throwing both names around in quick succession, and because they don't know the *gravity* of the situation they're not gonna be quiet.

They haven't actually *said* my name yet, so maybe they don't remember it? I certainly don't remember theirs. Kinda weird they haven't asked, but neither have I, so it could be fine... but I can't really fix that, can I? If I intro as Ash I'm gonna have all the same problems, with a bonus chance of just being overheard directly. If I use my deadname though, and they either remember my actual name or never forgot it and I'm just misreading them, they're going to find that very strange and want to talk to me all about it, again with normal speaking voices in an open conversation I am absolutely not willing to have. Will I tell them about my identity? Will I try to pass it off as a nickname? Why would I apply to a job under a nickname?

Where is my family? Outside? Hm. If this person is just not going to mention names at all, which is apparently what they're doing, I can at least let this conversation run its course (assuming the other person doesn't come back...) and then leave, and as long as I never see any of these people ever again, it'll be okay, I think.
