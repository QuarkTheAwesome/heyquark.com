---
title: Isolation Microblog 1
tags: personal brief micro me links
comments: true
---

It's official - I've got 7 days of isolation lined up due to a family member contracting COVID-19. I've decided I'm going to make a blog post every day of it. Since I've spent most of today trying to get my affairs in order given this sudden news, I don't have much, but I do have a thought that's been bugging me: I absolutely told someone recently "Oh, Axel F! I bet you it's the Crazy Frog version" when the song was in fact Popcorn. Which is. Embarrassing.

# Links
- [Popcorn - YouTube](https://www.youtube.com/watch?v=0EnXLQuAJj4) (decidedly Not the crazy frog version - this is Hot Butter's)
- [► BEST OF CHIPTUNE JANUARY 2022 ◄ ~(￣▽￣)~ - YouTube](https://www.youtube.com/watch?v=s_VcF1iEw90) (since when was Xefox posting again?)
- [Atari Sure Hoping No Big, Strong, Handsome Game Company Buys Them, Too - Hard Drive](https://hard-drive.net/atari-sure-hoping-no-big-strong-handsome-game-company-buys-them-too/)

# The Elephant in the (microblogging) Room
"Ash," you might be saying, "you *have* a Mastodon account. if all you really wanted was to get some random thought about Crazy Frog out there, why aren't you using that? It's literally built for microblogging."

Well... I dunno. I had an interaction on fedi last year which the other party pretty heavily subtooted me over, and I felt terrible about. I hope they understood my attempt to apologise - the last thing I wanted to do was to @ them *more* so it was a bit unorthodox. All parties involved have presumably completely forgotten about it, which is great, but after that I just didn't enjoy the idea of using fedi and looking at other people's posts anymore. I had already kinda known that social media isn't a net positive for me, and while fedi is better than Twitter it is still social media. I was happy to take this gap in the habit as an opportunity to quit it entirely, which I have.

I do still posts links to my blog since, well, I never really made any kind of statement about it so I suspect a lot of my friends still expect to hear from me that way. I don't really want to engage with the networking aspect of it though, looking at and boosting other posts, that kind of thing. If you want me to see your stuff you should talk to me directly or via a group chat, even if it's just a link.

In any case, until tomorrow, stay fresh! 🦑
