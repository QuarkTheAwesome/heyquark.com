---
# SPDX-FileCopyrightText: 2017 Ash Logan <ash@heyquark.com>
#
# SPDX-License-Identifier: CC-BY-4.0
title: "Origins of a Programmer: How I got started"
tags: me programming getting-started
comments: true
---
I've had a lot of people asking me how I first got started with programming, and how I got to where I am today. It's not a particularly wild ride (maybe I can make this sound better?) but hopefully this story of snakes, chat programs and Minecraft gives you some insight into my background.

I owe a lot to my Dad for getting me started with programming - I had always liked computers, but the first thing I remember around programming was [Snake Wrangling for Kids](https://www.utrgv.edu/cstem/_files/documents/snake-wrangling.pdf) - a PDF all about Python that Dad gave me. I remember struggling with getting the interpreter to run (some things never change), the excellent explanation of variables - comparing them to mailboxes - and *turtle graphics*, arguably the best name any programming library has ever had. I remember having aspirations of recreating Icy Tower with that turtle. :)

While I don't remember exactly what I ended up making beyond the examples given in that PDF, I know that it had quite an impact on me - I've forgotten the language, but not the book!

At some point in 2012, I saw a fellow student fiddling with Visual C# on a school computer. I hadn't programmed much for a while, but this caught my eye. I didn't really know what I was looking at, and through some strange chain of events I ended up sitting in front of a machine running Visual Basic instead (I later found out there were only two machines in the school with Visual C# installed; the rest had Basic). On the startpage was a link to some tutorials which took me through building a few simple things - a maze game, a picture viewer, etc. When I got confident enough to do my own project, I settled on a chat program - it wrote messages to text files in a network drive (complex, I know). I largely improvised the code - with autocomplete's help - and looked up things as I needed. Suffice to say, it wasn't pretty. At some point, I added things like categorisation of the afformention text files, and even attempted user verification. You can imagine how the failure of that last one created some drama on campus!

The project waned, and I started fiddling around with Minecraft - Bukkit plugins, specifically. You can still find some of these on my GitHub, actually. There isn't really much I can say about this point; just that the programming I did was largely improvised. I started by forking Rapsheet, a plugin that had been dormant for quite a while at that point, and adding a few features a server owner I was working with wanted. I improvised them together - reading the existing code and googling for specific solutions.

This is the same learning pattern I would follow with the Wii U - at the time, libwiiu was the big homebrew thing; so I took the button test example and through much bodging, guessing and failures I ended up adding the location of the analog sticks to the displayed information. The rest, as they say, is history.
