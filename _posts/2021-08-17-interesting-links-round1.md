---
title: Some Interesting Links
category: general
tags: personal links cool-stuff me
comments: true
---

One of the things I thought I might try to get myself blogging more (and to force me to actually appreciate things I like) is posting collections of links, things I found interesting or spent time on in the last [time period] - dare I say monthly? dangerous. This seems to be taking root as various YouTube folk start mailing lists, and of course some great newsletters (check out [Cryptography Dispatches](https://buttondown.email/cryptography-dispatches/)!) do the same. Without further ado, here are some interesting things I saw during the July-August-ish period for you to peruse.

## Portal Reloaded Soundtrack
I played through Portal Reloaded recently - it was extremely good; the puzzles really made me work for them in the best way. It's something I don't see much of in Workshop maps, though perhaps that's just because I usually play co-op from the "most recent" section - not exactly setting myself up for the highest quality!

I liked how the puzzles in Reloaded both took advantage of the immediate mechanics of time travel (setting things up in the past to affect the future), but also used it to create interesting challenges around what order you could do things in, requiring you to juggle delicate setups on both sides of the time portal, where one misstep in the past would break your future setup. The soundtrack is a lot less ambient than Portal 2's, and it stuck with me more because of it. Favourite track is One Step at a Time - not sure if that's for its own merits or because I spent so long listening to it in the last chamber~

If you know of Workshop maps that hit a similar difficulty, do [let me know](/aboutme)!

- [Portal Reloaded - Soundtrack](https://portanis.bandcamp.com/album/portal-reloaded-soundtrack) (bandcamp.com)

## I want off Mr. Golang's Wild Ride
Every now and again I enjoy reading a negative opinion piece on a niche topic - and on the chopping block this time was the programming language Go. An interesting tour through the API design of a language, future-proofing it and handling the lowest common denominator between platforms, with occasional diversions into RFC processes and dependency hell. If this wasn't the first time I'd made a post like this, [Soatok's AES-GCM design analysis](https://soatok.blog/2020/05/13/why-aes-gcm-sucks/) would have been in a previous post for similar reasons, so check that out too.

- [I want off Mr. Golang's Wild Ride](https://fasterthanli.me/articles/i-want-off-mr-golangs-wild-ride) (fasterthanli.me)

## I am a Java, C#, C or C++ developer, time to do some Rust
A very good read about Rust, something I've been wanting to get into for a little while. I had plans at the start of the year to do stuff with a Pi Pico I bought but that idea fell over; though I still take an interest in Rust Stuff when I see it. I did find out that exjam had also looked at Rust for a use a bit closer to home, and I must say, [the dream might be closer than I think!](https://github.com/QuarkTheAwesome/wut-rust-demo/blob/cdae373f6d98b8ffd86c09cc5cedccfe28d2f8d8/src/main.rs)

- [I am a Java, C#, C or C++ developer, time to do some Rust](https://fasterthanli.me/articles/i-am-a-java-csharp-c-or-cplusplus-dev-time-to-do-some-rust) (fasterthanli.me)

## Computer niceties
An interesting blogpost with lots of little computer tidbits, but my favourite thing (warranting its inclusion here) is the workaround for keyboard layouts in encrypted volumes - adding a new decryption key with your preferred layout transposed over qwerty is super inventive and makes me very happy.

- [Computer niceties](https://cadence.moe/blog/2021-07-30-computer-niceties) (cadence.moe)

## Static Stack Usage Analysis
I was debugging a very spicy RetroArch issue earlier this month and had a brief stint of believing it was a stack overflow which led me to this - GCC will apparently spit out stack usage info if you ask it nicely. I will have to come back to this at some point I think, maybe if I revisit the SRB2 wiiu port? Oh, and the "RetroArch issue" turned out to be [an emulation bug](https://github.com/QuarkTheAwesome/decaf-emu/commit/d5104d2093e83ebb886a23bcc99c98175880c323).

- [Static Stack Usage Analysis](https://gcc.gnu.org/onlinedocs/gnat_ugn/Static-Stack-Usage-Analysis.html) (gcc.gnu.org)

## M+ FONTS
The last WiiU related thing here, I swear, but I thought it was neat - after a request by vgmoose and some reverse font lookup, turns out the classic OSScreen "debug" font used in many a WiiU homebrew is "M+ M Type-2 Medium". If you visit the link, make sure to switch the preview to "Medium" for the specific font. It's strange seeing lighter variants of a font I've stared so much at - something feels very wrong about them. Still neat!

- [M+ FONTS - DESIGN](https://mplus-fonts.osdn.jp/design.html#mplus_m2) (mplus-fonts.osdn.jp)

## Testing Monopoly Tips with Python simulation
Last but not least, a neat video investigating the effects of various strategies and hot tips on the outcomes of Monopoly; featuring extensive simulation and some very nice graphs. It's fun to see the unintuitive ways changes in behaviour can affect a game like this.

- [Testing Monopoly Tips with Python simulation. Should you really ignore greens? (and much more)](https://www.youtube.com/watch?v=6EJrZeN0jNI) (youtube.com)

## Conclusion
I hope this provided some interesting things to look at. I will probably keep track of more interesting things for another post in the future - we'll see!

Stay fresh. 🦑
