---
# SPDX-FileCopyrightText: 2020 Ash Logan <ash@heyquark.com>
#
# SPDX-License-Identifier: CC-BY-4.0
title: "(Both) IPv6 Privacy Extensions"
category: sysadmin
tags: tech networking ipv6 linux
comments: true
---
One of the biggest benefits of IPv6 is that your computer's address is a real, actual, global IP, rather than being a super-secret private IP that only your LAN knows about. I'm most excited for port forwarding to be a process that makes sense and the benefits to p2p gaming that will result. I am, however, saddened by the inevitable death of "hey join my minecraft server it's 192.168.1.12". (Note that you can still have a firewall in your home router, as is the norm with v4!) Anyway, I digress. There are Privacy Issues that arise from this.

See, if your computer is to have a global address instead of a private one, there needs to be some way for a unique address to be assigned to you. While your router could dole out an address for you via DHCP, IPv6 would like things to be more automatic than that, and as such your router will tell you the shared prefix for your network (usually the first 56 or 64 bits) with your computer responsible for generating the rest.

In the original IPv6 specs, the way you'd generate the last half of your address was to take your *MAC address*, mangle it a bit, and use that. This is Bad since the MAC address is unique to your hardware, and thus Big Government can trivially track a particular piece of hardware's network activity, even if you stick to public hotspots or use a friend's connection or whatever. Similarly, one of the main defenses people use against predatory copyright subpoenas is "well, all you have is an IPv4, it could be an entire apartment building" - using MAC addresses would remove that protection, since we can now identify what particular laptop downloaded that torrent. So, problem!

 - [Prenda Law](https://en.wikipedia.org/wiki/Prenda_Law#History) - A well-known example of the predatory copyright racket, if you've not heard of it. This lot were fraudulent, but the same techniques are still used against actual torrenters!

Through many RFCs we have ended up with two similarly-named solutions to this problem, which is definitely too many, since as we'll see in Part 2 enabling only one of them can lead to some unexpected properties. They are:
- RFC8981, sometimes called IPv6 Privacy Extensions, NetworkManager's `ip6-privacy` option;
- RFC7217, Stable Privacy Addresses, `addr-gen-mode`.

First up is RFC8981, colloquially known as "IPv6 Privacy Extensions" - I like to call them random addresses. This says that your machine should just, pick a random number instead of a MAC address, and go from there. These random addresses have an expiry time on them, after which a new random address is chosen. To an observer, it looks like a device appears, does some traffic for a few hours, then vanishes, never to be seen again. If everyone on your network is doing this, it becomes a sea of random numbers that are near-impossible to corellate to any particular person or device without external hints. (This, by the way, is sometimes why you'll catch a machine with loads of IPv6 addresses - they're old ramdom/privacy ones that have expired, but are kept around for programs that still have a connection open using that address. Think long-running WebSockets, that type of thing.) The copyright troll is back to how it was under v4 - they only have accuracy down to the building level.

There are, however, problems with this - namely that it's almost useless for *incoming* connections. If you want to host a Minecraft server on your laptop, it'd be a PITA if the address kept changing each day or each time you restarted it - especially since that address is global now, and is what other players connect to!

Enter RFC7217, known as "Stable Privacy" addresses. These use *mumbles* cryptography *mumbles* hashes *mumbles* to generate an address from both your unchanging MAC and from the particular network you're connected to. So, at home, you have one permanent address, and at the coffee shop you have a different one, but both of these will not change over time. This is a cute solution to the Minecraft server problem that still prevents you from being tracked as you move between networks. However, it's now possible to track and corellate the network activity of a particular machine on a single network over time, so it's a weaker privacy guarantee.

In summary, there are two different kinds of "private" IPv6 addresses:
- random addresses, which change frequently and defeat tracking over time
- stable privacy addresses, which are unchanging for a given network and thus only defeat tracking across multiple networks

In Part 2, we'll see how NetworkManager can configure these things, and the very suprising default configuration on Fedora.
