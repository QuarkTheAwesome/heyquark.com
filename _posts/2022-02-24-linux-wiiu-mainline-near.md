---
title: linux-wiiu nearly ready for mainline
category: brew
tags: linux programming brief
comments: true
---

I've spent the last couple weeks cleaning up the platform bringup code for linux-wiiu to the point where I can send it upstream for review (and presumably several rounds of back-and-forth). It's getting excitingly close - now have a patchset that applies clean to mainline and prints dmesg to linux-loader:

<a href="/assets/img/2022-02-24/dmesg1.png" target="_blank"><picture>
    <source srcset="/assets/img/2022-02-24/dmesg1.webp" type="image/webp">
    <img src="/assets/img/2022-02-24/dmesg1.png" alt="Screenful of text, Linux 5.16.0 booting on wiiu (page 1)"/>
</picture></a>
<a href="/assets/img/2022-02-24/dmesg2.png" target="_blank"><picture>
    <source srcset="/assets/img/2022-02-24/dmesg2.webp" type="image/webp">
    <img src="/assets/img/2022-02-24/dmesg2.png" alt="Screenful of text, Linux 5.16.0 booting on wiiu (page 2)"/>
</picture></a>

I have a few more warnings to fix and devicetree nodes to document, but otherwise we're looking really good. Check out the External Links section on  [linux-wiiu's homepage](https://wiki.linux-wiiu.org/wiki/Main_Page) for Discord/Matrix/XMPP if you want to keep an eye on progress or get involved.

Stay fresh! {{"ok_gesture_b2" | m_emoji}} <img class="emoji" src="/assets/img/wave_emoji_2.svg" alt="[ocean wave emoji]" draggable="false">
