---
title: Minecraft 1.19 has made me scared of caves
tags: gaming minecraft me
comments: true
---
I went in to the update a bit later than most, only trying it out when my SMP of choice moved over. I had previously been playing in older 1.17 chunks, so I effectively got Caves and Cliffs at the same time too. I also, unlike most, went in *completely blind*. I knew that there was a difficult mob named the Warden, and something about frogs, and very little else.

I would later find out that the Deep Dark, the new biome added and key feature of 1.19, is quite rare. I, unfortunately, built my base right on top of one. So, as one does, I dug down, and got set up near lava level for some mining. I found a cave - much larger and more spacious than I was used to (thanks Caves and Cliffs) and explored it as it spiralled down below y level 0. The cave got thin, steep, crack-like, forcing me to sneak through one-block gaps. Down, far into this crack is where I was introduced to Sculk.

<a href="/assets/img/2022-07-08/smallcaves.png" target="_blank">
<img src="/assets/img/2022-07-08/smallcaves.png"
     alt="One such crack-like cave, dimly lit and with no room to move"
     srcset="/assets/img/2022-07-08/smallcaves-480.webp 853w,
             /assets/img/2022-07-08/smallcaves-720.webp 1280w,
             /assets/img/2022-07-08/smallcaves.webp 1920w"
     sizes="(max-width: 800px) 100vw, 60vw"/>
</a>

Sculk is a growth that appears in the Deep Dark, both replacing blocks and growing on top of it, dark and softly glowing. If you venture onto it, the squelchy, hollow sound of your footsteps may alert a flowing, moving block nearby (apparently called a "Sculk Sensor") - you can only watch as it visibly triggers another new block, which emits a loud scream. A low, distant rumbling sound responds - the in-game subtitles call this "Warden approaches".

The appearance of this strange, fungal growth at the bottom of an unfamiliar cave already had me right on edge, and I absolutely turned and ran at this. Minecraft is a unique game for how *quiet* it is, especially with the music off (as I prefer it), so it making non-random scary noises at you is something to take note of. But, like. I can't not know, right? There's a strange, otherworldly secret not even a hundred blocks below my base. I can't not test it.

<a href="/assets/img/2022-07-08/sculk.png" target="_blank">
<img src="/assets/img/2022-07-08/sculk.png"
     alt="The site of my first Warden encounter"
     srcset="/assets/img/2022-07-08/sculk-480.webp 853w,
             /assets/img/2022-07-08/sculk-720.webp 1280w,
             /assets/img/2022-07-08/sculk.webp 1920w"
     sizes="(max-width: 800px) 100vw, 60vw"/>
</a>

If you trigger the sensor again, the noise is described as "Warden draws near". I was too far in to avoid triggering it a third time while getting away.

Once the warden emerges from the rock, a few unexpected things happen. While it moves slowly and with hunched form, reminiscent of a stone statue come to life; it also blinds you - reducing the output of all light sources, such that even as you desperately throw down torches they only illuminate a measly block or two before the pitch black engulfs it. What you can see are the Warden's sonic attacks - waves of pressure that screech towards you at range and do unreal amounts of damage, killing in only two or three hits despite my strong armor.

Despite avoiding that area, I soon found the city - a cave opening up into a softly-lit stone metropolis, long overgrown by the Sculk I'd been developing a fear of. It extends for hundreds of blocks, and has the screaming, Warden-summoning blocks *everywhere*. My attempts to explore it didn't go well - in the end, I had to tackle it with my basemate using flight, temporarily enabled on the server as a perk for someone's donation. Flight kinda defeats the threat of treading too close to a sensor - I still have no idea how I'd tackle such a situation in vanilla Survival.

<a href="/assets/img/2022-07-08/city.png" target="_blank">
<img src="/assets/img/2022-07-08/city.png"
     alt="The forsaken city, dark and expansive"
     srcset="/assets/img/2022-07-08/city-480.webp 853w,
             /assets/img/2022-07-08/city-720.webp 1280w,
             /assets/img/2022-07-08/city.webp 1920w"
     sizes="(max-width: 800px) 100vw, 60vw"/>
</a>

I thought I would get used to Sculk - but I haven't. I've read up, I know about sneaking to avoid triggering the sensors, about the true rarity of the biome - but I haven't worked out the pure threat that is the Warden, and I still get tense at y levels too low, at a sculk sensor clicking as it hears me through a wall. It's, a weird experience! Totally unlike what I usually get from Minecraft, and I can't work out how I feel about it. The cave generation naturally draws me deeper, towards the threatening, the unknown.

So congratulations, Mojang, you actually made caves scary.
