---
title: Experiments in image corruption
category: programming
tags: rust images pc glitchart
comments: true
---
I recently saw a post from Daniel Aleksandersen about bitrot in modern image formats, which included a method to quantify how different a corrupted image is from its original. Daniel used random bitflips, but this got me thinking about glitch art - what if we could generate every possible corrupted image and quantitatively state which ones are the best?

- [Bitrot resistance of next-generation image formats - ctrl.blog](https://www.ctrl.blog/entry/bitrot-avif-jxl-comparison.html)

## The method
Okay, so let's just generate every corruption of some image file and run it through that algorithm. By "corruption", I mean bitflips, so for a.. randomly selected test image:

<a href="/assets/img/2022-02-08/promo-base.jpg" target="_blank"><picture>
    <source srcset="/assets/img/2022-02-08/promo-base.webp" type="image/webp">
    <img src="/assets/img/2022-02-08/promo-base.jpg" alt="A Mario Kart 8 promotional image"/>
</picture></a>

That's 173KiB, so 1.417×10^6 bits, which is.. how many bitflips?

<picture>
    <source srcset="/assets/img/2022-02-08/1417e6-factorial.webp" type="image/webp">
    <img class="small" src="/assets/img/2022-02-08/1417e6-factorial.png" alt="A Wolfram Alpha calculation showing that 1.417×10^6 factorial is 1.869×10^8101099"/>
</picture>

Nice. Given that the DSSIM algorithm used in the original post, which I would also like to use, takes a few hundred milliseconds to process an image, I'm sure we'll get some rad glitch art at around the time of the heat death of the universe.

Clearly we need a better strategy, so how about only flipping one bit at a time?

## bitflipper - a program to generate somewhat interesting glitch art
bitflipper is a program I wrote to take an image, test each possible bitflip (each file only differs from the original by one bit), and see if dssim thinks it's interesting. You can set the starting and ending range of the search, and a threshold for dssim. Any images deemed cool get saved for you to look over later and pick out your favorites - thanks to the analysis, there's no duplicates of the original or plain corrupt images in the output folder.

Since dssim is a Rust library, I also wrote bitflipper in Rust, a language I've been trying to learn more of anyway. I quite liked the library ecosystem and being able to Just Add Stuff (which can be quite the ordeal in C/C++, even when using submodules), though dealing with different kinds of Results and `?` was a bit strange and felt ugly. I'm sure there's some secret technique I don't understand yet.

Once all was said and done though, the code only ended up being 150 lines in a single file, including guff like parsing the commandline arguments, which is pretty cool considering the complexity of what it actually Does. I like!

- [bitflipper's main source file - GitLab](https://gitlab.com/QuarkTheAwesome/bitflipper/-/blob/main/src/main.rs)
- [bitflipper - GitLab](https://gitlab.com/QuarkTheAwesome/bitflipper)
- [dssim image similarity library (kornelski/dssim) - GitHub](https://github.com/kornelski/dssim)

## How well does it work?
Running on the above test image on default settings for an hour or so, bitflipper wrote out a few hundred images. Since each bitflip is sequential, they tend to cluster together in different categories, with one corruption getting more and more intense or moving around in the image.

Let's look at some of the results! Remember that each of these files only differ from the original JPEG by a single flipped bit.

### "Classical" JPEG corruption
This is the kind of stuff you expect to see when you hear about a half-overwritten image file or a errant cosmic ray corrupting some photographer's collection. It's pretty cool, but a bit overdone.

*Note*: Some of these .jpg files are "unstable" and render differently on different systems. Where I've noticed instability, the embedded images are re-encodes of how they appear for me, but you can click to see the original .jpg.

<a href="/assets/img/2022-02-08/classic-1.jpg" target="_blank"><picture>
    <source srcset="/assets/img/2022-02-08/classic-1.webp" type="image/webp">
    <img src="/assets/img/2022-02-08/classic-1.png" alt="An image with large, horizontal, coloured bands covering the entire frame"/>
</picture></a>
<a href="/assets/img/2022-02-08/classic-2.jpg" target="_blank"><picture>
    <source srcset="/assets/img/2022-02-08/classic-2.webp" type="image/webp">
    <img src="/assets/img/2022-02-08/classic-2.png" alt="An image that vaguely resembles the original, with colours affected in large bands and pixels offset left or right. The squid eye's from the original are still visible"/>
</picture></a>
<a href="/assets/img/2022-02-08/classic-3.jpg" target="_blank"><picture>
    <source srcset="/assets/img/2022-02-08/classic-3.webp" type="image/webp">
    <img src="/assets/img/2022-02-08/classic-3.png" alt="An almost unrecognisable image, with maybe the vaguest outline of the original in some places, but is otherwise dominated by striking colour bands"/>
</picture></a>

### Weird Macroblocking Stuff
In these ones, the structure of the JPEG file itself starts to show itself, with unique corruptions inside each compression block. There's some unique effects here that I haven't seen before!

<a href="/assets/img/2022-02-08/macroblock-1.jpg" target="_blank"><picture>
    <source srcset="/assets/img/2022-02-08/macroblock-1.webp" type="image/webp">
    <img src="/assets/img/2022-02-08/macroblock-1.jpg" alt="An image with increased contrast and strange artefacting in a grid shape"/>
</picture></a>
<a href="/assets/img/2022-02-08/macroblock-2.jpg" target="_blank"><picture>
    <source srcset="/assets/img/2022-02-08/macroblock-2.webp" type="image/webp">
    <img src="/assets/img/2022-02-08/macroblock-2.jpg" alt="An image with even more contrast, blowing out the blacks, and with each grid square almost a single colour"/>
</picture></a>
<a href="/assets/img/2022-02-08/macroblock-3.jpg" target="_blank"><picture>
    <source srcset="/assets/img/2022-02-08/macroblock-3.webp" type="image/webp">
    <img src="/assets/img/2022-02-08/macroblock-3.jpg" alt="An image with vertical artefact lines in each block, with pixels being shifted up or down within each column"/>
</picture></a>
<a href="/assets/img/2022-02-08/macroblock-4.jpg" target="_blank"><picture>
    <source srcset="/assets/img/2022-02-08/macroblock-4.webp" type="image/webp">
    <img src="/assets/img/2022-02-08/macroblock-4.jpg" alt="An image with horizontal bands of corruption and noise, making the image hard to make out behind the artefacts"/>
</picture></a>

### Colours
These ones play with the intensity of colours, almost making straight lines look curved. Very cool {{"content" | m_emoji}}

<a href="/assets/img/2022-02-08/colour-1.jpg" target="_blank"><picture>
    <source srcset="/assets/img/2022-02-08/colour-1.webp" type="image/webp">
    <img src="/assets/img/2022-02-08/colour-1.png" alt="An image with significantly oversaturated colours, causing a bloom effect around some objects"/>
</picture></a>
<a href="/assets/img/2022-02-08/colour-2.jpg" target="_blank"><picture>
    <source srcset="/assets/img/2022-02-08/colour-2.webp" type="image/webp">
    <img src="/assets/img/2022-02-08/colour-2.png" alt="A completely absurd image, with completely nonsensical colour information in a grid. The brightness is preserved, however, so the features of the image can still be made out"/>
</picture></a>

Lastly, my absolute favourite image:

<a href="/assets/img/2022-02-08/chrom-abbr.jpg" target="_blank"><picture>
    <source srcset="/assets/img/2022-02-08/chrom-abbr.webp" type="image/webp">
    <img src="/assets/img/2022-02-08/chrom-abbr.jpg" alt="An image mostly in grayscale, with some shifted pixels being a subtle red or cyan, similar to a chromatic abberation effect"/>
</picture></a>

[Let me know](/aboutme) if you have a go and get cool results, I'd love to see them.

Until next time! {{"crt_test_pattern" | m_emoji}}
