---
title: Isolation Microblog 2
tags: personal brief micro me
comments: true
---

It's day 2 of my 7-day isolation, so time for another microblog! I tested positive pretty recently and now get to sit at home, yay {{ "unhappy" | m_emoji }}. 

## Element Matrix and its notification sound
The reference client for the Matrix chat protocols is called Element, such a transcendentally generic name that I don't think I've ever had luck searching it without specifying "element matrix" - which is *still* pretty generic!
One thing about Element that does leave a very strong impression is the notification sound. It's loud, intrusive, and even anxiety-inducing.

Today [Cadence](https://cadence.moe) and I sat down - they made a userscript to replace the sound, and I mucked around trying to make one.
I took a bit of inspiration from Telegram Desktop's sound. It's smoother, slower, based on a chord rather than a noise; and notably only plays once - no matter how many messages you receive. I mucked around in Ableton with one of the electric piano sounds, added some saturation and a filter, and the end result turned out pretty nice!

- [My new notification sound](/assets/img/2022-07-04/notif.ogg) (available under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/))
- [Cadence's post about the script](https://cadence.moe/blog/2022-07-04-userscript-to-fix-notification-sounds-in-element-matrix)

## NDI being a closed standard kinda sucks
[NDI](https://ndi.tv/) is an ultra-low latency zero-configuration video streaming protocol, used a lot in the production world (think live shows) for getting video passed around over a LAN. It's *very* nice for streamers too, and anyone who's struggled with a laptop and a HDMI capture card may do well to investigate the OBS plugin, which can send and receive video over the network.
The thing is, NDI is owned by NewTek, who provides a royalty-free SDK and not much else. It's closed source and uses a custom video encoding - now added to ffmpeg, at least, but no hardware accel for you. NewTek's SDK is also kinda CPU-hungry, which rules out some low-power devices like the Raspberry Pis.

As best I can tell though, there *is no* standardised open way to do what NDI does. There's things that get close - Avahi, SAP, SDP, RTP all provide pieces of this funcionality, but nothing brings it together to say "this is a spec you can implement to receive/send a stream". Even if such a spec existed, good luck ever getting it supported in the myriad of commercial software packages that bank on NDI right now.
I'm mostly just unhappy about it not working on the Raspberry Pi, to be real. If you do know of a spec, please [let me know](/aboutme).

Stay fresh. 🦑
