# SPDX-FileCopyrightText: 2018 Ash Logan <ash@heyquark.com>
#
# SPDX-License-Identifier: MIT

module Jekyll
    module MutantEmoji
        def load_json()
            f = File.open("assets/img/mutant/mtnt_0.4.0_data.json")
            j = JSON.parse(f.read)
            f.close()
            j
        end
        def m_emoji(input)
            emoji = load_json().select{ |o| o["code"] == input }[0]
            if emoji == nil
                puts "WARNING: invalid emoji #{input}!"
                return ""
            end
            # The src tag seems a litte. odd. basically run $(dir) on it
            src = emoji["src"]
            src.slice!(src.split("/").last)
            "<img class=\"emoji\" src=\"/assets/img/mutant/#{src}#{emoji["code"]}.svg\" alt=\"[#{emoji["desc"]} emoji]\" draggable=\"false\">"
        end
    end
end

Liquid::Template.register_filter(Jekyll::MutantEmoji)
